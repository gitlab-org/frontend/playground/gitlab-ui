import examples from './examples';
import description from './link.md';

export default {
  description,
  examples,
  bootstrapComponent: 'b-link',
};
