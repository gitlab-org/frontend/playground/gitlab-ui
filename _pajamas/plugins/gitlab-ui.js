import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';

import * as gitlabCharts from '../../_gitlab-ui/charts';
import GlExampleDisplay from '../../_gitlab-ui/documentation/components/example_display.vue';
import GlExampleExplorer from '../../_gitlab-ui/documentation/components/example_explorer.vue';
import * as gitlabComponents from '../../_gitlab-ui/index';

Vue.use(BootstrapVue);
Vue.use(gitlabComponents.GlToast); // The toast plugin needs to be registered before being used

Object.entries({ ...gitlabComponents, ...gitlabCharts, GlExampleExplorer, GlExampleDisplay })
  .filter(([componentName]) => !componentName.includes('Directive'))
  .forEach(([componentName, component]) => Vue.component(componentName, component));
